FROM python:3 AS build-env
RUN groupadd --gid 1000 appuser \
    && useradd --system --home-dir /tmp --no-create-home --uid 1000 --gid 1000 appuser
COPY --chown=appuser:appuser . /build
USER appuser
RUN pip install \
        --target /build/netbox/lib \
        --requirement /build/requirements.txt \
        --requirement /build/requirements-docker.txt

#FROM gcr.io/distroless/python3
FROM python:3
RUN groupadd --gid 1000 appuser \
    && useradd --system --home-dir /app --no-create-home --uid 1000 --gid 1000 appuser
COPY --from=build-env --chown=appuser:appuser /build/netbox /app
USER appuser
ENV PYTHONPATH=$PYTHONPATH:/app/lib
ENV PATH=$PATH:/app/lib/bin
WORKDIR /app
