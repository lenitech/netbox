import os
import logging
import ecs_logging
import base64

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    "formatters": {
        "ecs_logging": {"()": ecs_logging.StdlibFormatter}
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'ecs_logging',
        },
    },
    'loggers': {
        '': {
            'handlers': ['console'],
            'level': 'INFO',
        },
    },
}

ALLOWED_HOSTS = [os.getenv('NETBOX_HOSTNAME')]
SECRET_KEY = os.getenv('NETBOX_SECRET_KEY')
LOGIN_REQUIRED = True
DATABASE = {
    'HOST': os.getenv('NETBOX_DB_HOST'),
    'PORT': int(os.getenv('NETBOX_DB_PORT')),
    'USER': os.getenv('NETBOX_DB_USERNAME'),
    'NAME': os.getenv('NETBOX_DB_NAME'),
    'CONN_MAX_AGE': 300,
    'OPTIONS': {
           'sslmode': 'verify-full',
           'sslrootcert': '/app/.egress-tls/issuing_ca',
           'sslcert': '/app/.egress-tls/tls.crt',
           'sslkey': '/app/.egress-tls/tls.key',
    }
}

REDIS = {
    'tasks': {
        'HOST': os.getenv('NETBOX_REDIS_HOST'),
        'PORT': int(os.getenv('NETBOX_REDIS_PORT')),
        'PASSWORD': os.getenv('NETBOX_REDIS_PASSWORD'),
        # Comment out `HOST` and `PORT` lines and uncomment the following if using Redis Sentinel
        # 'SENTINELS': [('mysentinel.redis.example.com', 6379)],
        # 'SENTINEL_SERVICE': 'netbox',
        'DATABASE': int(os.getenv('NETBOX_REDIS_DATABASE')),
        'SSL': False,
    },
    'caching': {
        'HOST': os.getenv('NETBOX_CACHE_HOST'),
        'PORT': int(os.getenv('NETBOX_CACHE_PORT')),
        'PASSWORD': os.getenv('NETBOX_CACHE_PASSWORD'),
        # Comment out `HOST` and `PORT` lines and uncomment the following if using Redis Sentinel
        # 'SENTINELS': [('mysentinel.redis.example.com', 6379)],
        # 'SENTINEL_SERVICE': 'netbox',
        'DATABASE': int(os.getenv('NETBOX_CACHE_DATABASE')),
        'SSL': False,
    }
}

REMOTE_AUTH_BACKEND = 'social_core.backends.keycloak.KeycloakOAuth2'
SOCIAL_AUTH_KEYCLOAK_ID_KEY = 'email'
SOCIAL_AUTH_KEYCLOAK_KEY = os.getenv('NETBOX_CLIENT_ID')
SOCIAL_AUTH_KEYCLOAK_SECRET = os.getenv('NETBOX_CLIENT_SECRET')
SOCIAL_AUTH_KEYCLOAK_PUBLIC_KEY = os.getenv('NETBOX_PUBLIC_KEY')
SOCIAL_AUTH_KEYCLOAK_AUTHORIZATION_URL = os.getenv('NETBOX_AUTH_URL')
SOCIAL_AUTH_KEYCLOAK_ACCESS_TOKEN_URL = os.getenv('NETBOX_TOKEN_URL')
REMOTE_AUTH_HEADER = 'HTTP_REMOTE_USER'
REMOTE_AUTH_AUTO_CREATE_USER = True
REMOTE_AUTH_GROUP_SYNC_ENABLED = True
