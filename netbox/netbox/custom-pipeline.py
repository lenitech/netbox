from django.contrib.auth.models import Group

class AuthFailed(Exception):
    pass

def set_role(response, user, backend, *args, **kwargs):
    '''
    Get roles from JWT
    Set is_superuser or is_staff for special roles 'superuser' and 'staff'
    '''
    try:
        roles = response['roles']
    except KeyError:
        raise AuthFailed("No role assigned")

    try:
        user.is_superuser = False
        user.is_staff = False

        if 'superuser' in roles:
            user.is_superuser = True
        if 'staff' in roles:
            user.is_staff = True
        user.save()
    except:
        pass
