import os
from opentelemetry.instrumentation.wsgi import OpenTelemetryMiddleware

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "netbox.settings")

application = get_wsgi_application()
application = OpenTelemetryMiddleware(application)
